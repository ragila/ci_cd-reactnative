import React from 'react';
import {connect} from 'react-redux';
import {createStackNavigator} from '@react-navigation/stack';
import MainNavigator from './MainNavigator';
import LoginScreen from '../screens/Login';
import {NavigationContainer} from '@react-navigation/native';

const Stack = createStackNavigator();

function AppStack(props) {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        {props.sudahLogin ? (
          <Stack.Screen
            name="Main"
            component={MainNavigator}
          />
        ) : (
          <Stack.Screen
            name="Login"
            component={LoginScreen}
          />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const mapStateToProps = (state) => ({
  sudahLogin: state.auth.isLoggedIn,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(AppStack);

import {combineReducers} from 'redux'
import todos from './todo'
import auth from './auth'
import profile from './profile'
import interests from './interest'

export default combineReducers({
    todos,
    auth,
    profile,
    interests
})
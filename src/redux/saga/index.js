import {all} from 'redux-saga/effects';
import authSaga from './auth';
import profileSaga from './profile';
import interestsSaga from './interests.saga'

export default function* rootSaga() {
  yield all([authSaga(), profileSaga(), interestsSaga()]);
}

import React from 'react'
import {shallow, configure} from 'enzyme'
import Button from '../src/component/Button'
import Adapter from 'enzyme-adapter-react-16'

configure({adapter: new Adapter()})

describe('Button', () => {
    it('should contain same label', () => {
        const wrapper = shallow (<Button label= 'test label'/>)
        expect(wrapper.contains('test label')).toEqual(true)
    })
})
import React from 'react';
import {SafeAreaView, StatusBar, Text} from 'react-native';

import {Provider} from 'react-redux';
import AppStack from './navigator/AppStack';
import store from './store';

export default function App() {
  return (
    <Provider store={store}>
      {/* <StatusBar barStyle="dark-content" /> */}
      <AppStack />
    </Provider>
  );
}

let nexTodoId= 0;
export const addTodo = ((text) => ({
    type    : 'ADD TODO',
    id      : nexTodoId++,
    text,
}))
import React, {useEffect} from 'react';
import { StyleSheet, Text, View } from 'react-native';
import {connect} from 'react-redux'
import { getInterests } from '../redux/action/interests.action';

function BlankPage(props) {
  
  useEffect(() => {
    props.ambilInterests()
  }, [])

  return (
    <View style={styles.container}>
      <Text>
        {JSON.stringify(props.interests)}
      </Text>
    </View>
  );
}

const mapStateToProps = (state) => ({
  interests: state.interests
});

const mapDispatchToProps = (dispatch) => ({
  ambilInterests: () => dispatch(getInterests()) 
});

export default connect(mapStateToProps, mapDispatchToProps)(BlankPage);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center'
  }
});

const initialState = {  
  user_id: '',
  interests: []
};

const interests = (state = initialState, action) => {
  switch (action.type) {
    // case 'GET_INTERESTS': {
    //     return {
    //       ...state,
    //       ...action.payload,
    //     };
    //   }
    case 'GET_INTERESTS_SUCCESS': {
        return {
          ...state,
          ...action.payload,
        };
      }
    case 'GET_INTERESTS_FAILED': {
      return {
        ...state,
        ...action.payload,
      };
    }
    default:
      return state;
  }
};

export default interests;

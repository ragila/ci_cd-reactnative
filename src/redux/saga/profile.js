import {takeLatest, put} from 'redux-saga/effects';
import {apiFetchProfileDetail} from '../../common/api/profile';
import {ToastAndroid} from 'react-native';
import {getAccountId, getHeaders} from '../../common/function/auth';
import {
  GET_PROFILE,
  GET_PROFILE_SUCCESS,
  GET_PROFILE_FAILED,
} from '../action/profile_types';

function* getProfileDetail() {
  try {
    console.info('cobaaa')
    const headers = yield getHeaders();
    const accountId = yield getAccountId();

    // FETCH PROFILE DATA
    const resProfile = yield apiFetchProfileDetail(accountId, headers);
    yield put({type: GET_PROFILE_SUCCESS, payload: resProfile.data});
  } catch (e) {
    // show alert
    ToastAndroid.showWithGravity(
      'Gagal mengambil data profil',
      ToastAndroid.SHORT,
      ToastAndroid.BOTTOM,
    );

    yield put({type: GET_PROFILE_FAILED});
  }
}

function* profileSaga() {
  yield takeLatest(GET_PROFILE, getProfileDetail);
}

export default profileSaga;

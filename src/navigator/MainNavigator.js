import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
const Tab = createBottomTabNavigator();

import BlankPage from '../screens/BlankPage';
import TodoScreen from '../screens/TodoScreen';

import DashboardStack from './DashboardStack';

export default function MainNavigator() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Dashboard" component={DashboardStack} />
      <Tab.Screen name="Example" component={BlankPage} />
      <Tab.Screen name="Todo" component={TodoScreen} />
    </Tab.Navigator>
  );
}
